package com.henok.hw.adapters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.henok.hw.R;
import com.henok.hw.R.drawable;
import com.henok.hw.R.id;
import com.henok.hw.R.layout;
import com.henok.hw.utils.MD5Util;
import com.henok.hw.utils.ParseConstants;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

public class UserAdapter extends ArrayAdapter<ParseUser> {

	protected Context mContext;
	protected List<ParseUser> mUsers;

	public UserAdapter(Context context, List<ParseUser> users) {
		super(context, R.layout.message_item, users);
		mContext = context;
		mUsers = users;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.user_item, null);
			holder = new ViewHolder();
			holder.userImageView = (ImageView) convertView
					.findViewById(R.id.userImageView);
			holder.usernameLabel = (TextView) convertView
					.findViewById(R.id.usernameLabel);
			holder.userLocationLabel = (TextView) convertView
					.findViewById(R.id.userLocationLabel);
			holder.checkImageView = (ImageView) convertView
					.findViewById(R.id.checkedImageView);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		ParseUser user = mUsers.get(position);
		String email = user.getEmail().toLowerCase();

		if (email.equals("")) {
			holder.userImageView.setImageResource(R.drawable.avatar_empty);
		} else {
			String hash = MD5Util.md5Hex(email);
			String gravatarUrl = "http://www.gravatar.com/avatar/" + hash
					+ "?s=100&d=404";
			Picasso.with(mContext).load(gravatarUrl)
					.placeholder(R.drawable.avatar_empty)
					.into(holder.userImageView);
		}

		// TODO

		//List<String> loc = user.getList("userLocation");

		//String location = loc.get(0);

		String username = user.getUsername();
		if (username.length() > 20) {
			username = username.substring(0, 20);
		}
		holder.usernameLabel.setText(username);
		
		
	

		
		//holder.userLocationLabel.setText(location);
		// ParseObject locationObj =
		// ParseUser.getCurrentUser().getParseObject(ParseConstants.USER_LOCATION);
		//
		// List<String> locationArray =
		// locationObj.getList(ParseConstants.USER_LOCATION);
		//
		// String userLocation = locationArray.get(0);
		//
		// if (user.getString(ParseConstants.KEY_FILE_TYPE).equals(
		// ParseConstants.TYPE_IMAGE)) {
		// holder.iconImageView.setImageResource(R.drawable.ic_action_picture);
		// } else {
		// holder.iconImageView
		// .setImageResource(R.drawable.ic_action_play_over_video);
		// }
		// holder.userLocationLabel.setText(userLocation);

		GridView gridView = (GridView) parent;
		if (gridView.isItemChecked(position)) {
			holder.checkImageView.setVisibility(View.VISIBLE);
		} else {
			holder.checkImageView.setVisibility(View.INVISIBLE);
		}

		return convertView;
	}

	private static class ViewHolder {
		ImageView userImageView;
		ImageView checkImageView;
		TextView usernameLabel;
		TextView userLocationLabel;
		// TextView userLocationLabel;
	}

	public void refill(List<ParseUser> users) {
		mUsers.clear();
		mUsers.addAll(users);
		notifyDataSetChanged();
	}
}
