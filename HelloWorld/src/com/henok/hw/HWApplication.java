package com.henok.hw;

import android.app.Application;

import com.henok.hw.ui.HomeActivity;
import com.henok.hw.ui.InboxActivity;
import com.henok.hw.utils.ParseConstants;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;
import com.parse.PushService;

public class HWApplication extends Application {
	
	@Override
	public void onCreate() { 
		super.onCreate();
		Parse.initialize(this, "PEH6eku4WH5Glq8NItvdYSiwrgqsoXJYRUz7ggyj", "F0SuI2hvGbJ4fUEzZ4R761z6aKISH03I5pY2vMlY");
		//ParseTwitterUtils.initialize("cRuN9KyyyR20WXcuZEdOHLka4", "NnvuxsXqD83XIWtirqE4b3iKTMzAEm03KB3Pyl8lRJzT7nSpkq");
		//PushService.setDefaultPushCallback(this, InboxActivity.class);
		PushService.setDefaultPushCallback(this, InboxActivity.class, R.drawable.ic_launcher_reverse);
	}
	
	public static void updateParseInstallation(ParseUser user){
		ParseInstallation installation = ParseInstallation.getCurrentInstallation();
		installation.put(ParseConstants.KEY_USER_ID, user.getObjectId());
		installation.saveInBackground();
	}
}
