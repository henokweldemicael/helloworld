package com.henok.hw.ui;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.henok.hw.R;
import com.henok.hw.adapters.UserAdapter;
import com.henok.hw.utils.ParseConstants;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

public class FriendsActivity extends Activity {
	public static final String TAG = FriendsActivity.class.getSimpleName();

	protected ParseRelation<ParseUser> mFriendsRelation;
	protected ParseUser mCurrentUser;
	protected List<ParseUser> mFriends;
	protected GridView mGridView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_grid);
		
		getActionBar().setIcon(R.drawable.ic_launcher_reverse);

		mGridView = (GridView) findViewById(R.id.friendsGrid);

		TextView emptyTextView = (TextView) findViewById(R.id.empty);
		emptyTextView.setVisibility(View.VISIBLE);
		mGridView.setEmptyView(emptyTextView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.friends, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();

		switch (itemId) {
		case R.id.menu_setting:
			Intent cameraIntent = new Intent(this, HomeActivity.class);
			startActivity(cameraIntent);
			break;
		case R.id.menu_logout:
			ParseUser.logOut();
			navigateToLogin();
			break;
		case R.id.menu_edit_friends:
			Intent intent = new Intent(this, EditFriendsActivity.class);
			startActivity(intent);
			break;
		case R.id.menu_friends:
			Intent friendsIntent = new Intent(this, FriendsActivity.class);
			startActivity(friendsIntent);
			break;

		case R.id.menu_inbox:
			Intent inboxIntent = new Intent(this, InboxActivity.class);
			startActivity(inboxIntent);
			break;

		}

		return super.onOptionsItemSelected(item);
	}

	private void navigateToLogin() {
		Intent intent = new Intent(this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}

	@Override
	public void onResume() {
		super.onResume();

		mCurrentUser = ParseUser.getCurrentUser();
		mFriendsRelation = mCurrentUser
				.getRelation(ParseConstants.KEY_FRIENDS_RELATION);

		setProgressBarIndeterminateVisibility(true);

		ParseQuery<ParseUser> query = mFriendsRelation.getQuery();
		query.addAscendingOrder(ParseConstants.KEY_USERNAME);
		query.findInBackground(new FindCallback<ParseUser>() {
			@Override
			public void done(List<ParseUser> friends, ParseException e) {
				setProgressBarIndeterminateVisibility(false);

				if (e == null) {
					mFriends = friends;

					String[] usernames = new String[mFriends.size()];
					int i = 0;
					for (ParseUser user : mFriends) {
						usernames[i] = user.getUsername();
						i++;
					}
					
					//Adapter
					if (mGridView.getAdapter() == null) {
						UserAdapter adapter = new UserAdapter(
								FriendsActivity.this, mFriends);
						//setListAdapter(adapter);
						mGridView.setAdapter(adapter);
					}else{
						((UserAdapter)mGridView.getAdapter()).refill(mFriends);
					}

				} else {
					Log.e(TAG, e.getMessage());
					AlertDialog.Builder builder = new AlertDialog.Builder(
//							getListView().getContext());
						    mGridView.getContext());
					builder.setMessage(e.getMessage())
							.setTitle(R.string.error_title)
							.setPositiveButton(android.R.string.ok, null);
					AlertDialog dialog = builder.create();
					dialog.show();
				}
			}
		});

	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.user_grid,
					container, false);
			return rootView;
		}
	}

}
