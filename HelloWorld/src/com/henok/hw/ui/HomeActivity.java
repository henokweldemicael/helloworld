package com.henok.hw.ui;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import com.henok.hw.R;
import com.henok.hw.utils.ParseConstants;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class HomeActivity extends Activity {

	private String TAG = HomeActivity.class.getSimpleName();
	private SurfaceView preview = null;
	private SurfaceHolder previewHolder = null;
	private Camera camera = null;
	private boolean inPreview = false;
	private boolean cameraConfigured = false;
	private LayoutInflater controlInflater = null;

	private ImageButton switchCameraBtn;
	private ImageButton takePictureBtn;
	private ImageButton gotoInboxBtn;
	private ImageButton gotoFriendsBtn;
	private int currentCameraId = 0;
	Camera.CameraInfo info = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		getActionBar().setIcon(R.drawable.ic_launcher_reverse);


		
		preview = (SurfaceView) findViewById(R.id.preview);
		previewHolder = preview.getHolder();
		previewHolder.addCallback(surfaceCallback);
		previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		controlInflater = LayoutInflater.from(getBaseContext());
		View viewSwitchCamera = controlInflater.inflate(
				R.layout.switch_camera_overlay, null);
		View viewControls = controlInflater.inflate(R.layout.controls, null);
		LayoutParams layoutParamsControl = new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		this.addContentView(viewSwitchCamera, layoutParamsControl);
		this.addContentView(viewControls, layoutParamsControl);

		takePictureBtn = (ImageButton) findViewById(R.id.takePictureBtn);
		takePictureBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// if (inPreview) {

				camera.takePicture(null, null, mPicture);

				inPreview = true; // finish();

			}
		});

	}

	@Override
	public void onResume() {
		super.onResume();

		// // // Navigate to inbox.
		// gotoInboxBtn = (ImageButton) findViewById(R.id.gotoInboxBtn);
		// gotoInboxBtn.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View arg0) {
		// Intent inboxIntent = new Intent(HomeActivity.this,
		// InboxActivity.class);
		// startActivity(inboxIntent);
		//
		// }
		// });
		//
		// // // Navigate to Friends.
		// gotoFriendsBtn = (ImageButton) findViewById(R.id.gotoFriendsBtn);
		// gotoFriendsBtn.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View arg0) {
		// Intent friendsIntent = new Intent(HomeActivity.this,
		// FriendsActivity.class);
		// startActivity(friendsIntent);
		//
		// }
		// });

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			try {
				Camera.CameraInfo info = new Camera.CameraInfo();
				for (int i = 0; i < camera.getNumberOfCameras(); i++) {
					Camera.getCameraInfo(i, info);

					if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
						camera = Camera.open(i);
					}
				}

				currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
				camera.setDisplayOrientation(90);

				camera.setPreviewDisplay(previewHolder);

				camera.startPreview();

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (camera == null) {
			try {
				camera = Camera.open();

				camera.setDisplayOrientation(90);

				camera.setPreviewDisplay(previewHolder);

				camera.startPreview();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// =======================================================

		switchCameraBtn = (ImageButton) findViewById(R.id.switchCameraBtn);
		// if phone has only one camera, hide "switch camera" button
		if (Camera.getNumberOfCameras() == 1) {
			switchCameraBtn.setVisibility(View.INVISIBLE);
		} else {
			switchCameraBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// if (inPreview) {
					// camera.stopPreview();
					// }
					// NB: if you don't release the current camera before
					// switching, you app will crash
					camera.release();

					// swap the id of the camera to be used
					if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {

						currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;

					} else {

						// if (info.facing ==
						// Camera.CameraInfo.CAMERA_FACING_BACK) {

						currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;

					}
					try {
						camera = Camera.open(currentCameraId);

						camera.setDisplayOrientation(90);

						camera.setPreviewDisplay(previewHolder);

						camera.startPreview();

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});
		}

		// =======================================================
	}

	@Override
	public void onPause() {
		if (inPreview) {
			camera.stopPreview();
		}

		camera.release();
		camera = null;
		inPreview = false;

		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.main, menu);

		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// if (item.getItemId() == R.id.camera) {
		// if (inPreview) {
		// camera.takePicture(null, null, photoCallback);
		// inPreview = false;
		// }
		// }
		//
		// return (super.onOptionsItemSelected(item));
		int itemId = item.getItemId();

		switch (itemId) {
		case R.id.menu_setting:
			Intent cameraIntent = new Intent(this, SettingActivity.class);
			startActivity(cameraIntent);
			break;
		case R.id.menu_logout:
			ParseUser.logOut();
			navigateToLogin();
			break;
		case R.id.menu_edit_friends:
			Intent intent = new Intent(this, EditFriendsActivity.class);
			startActivity(intent);
			break;
		case R.id.menu_friends:
			Intent friendsIntent = new Intent(this, FriendsActivity.class);
			startActivity(friendsIntent);
			break;

		case R.id.menu_inbox:
			Intent inboxIntent = new Intent(this, InboxActivity.class);
			startActivity(inboxIntent);
			break;

		}

		return super.onOptionsItemSelected(item);
	}

	private void navigateToLogin() {
		Intent intent = new Intent(this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}

	private Camera.Size getBestPreviewSize(int width, int height,
			Camera.Parameters parameters) {
		Camera.Size result = null;

		for (Camera.Size size : parameters.getSupportedPreviewSizes()) {

			if (size.width <= width && size.height <= height) {
				if (result == null) {
					result = size;
					System.out.println(width);
					System.out.println(height);
				} else {
					int resultArea = result.width * result.height;
					int newArea = size.width * size.height;

					if (newArea > resultArea) {
						result = size;
					}
				}
			}
		}

		return (result);
	}

	private Camera.Size getSmallestPictureSize(Camera.Parameters parameters) {
		Camera.Size result = null;

		for (Camera.Size size : parameters.getSupportedPictureSizes()) {
			if (result == null) {
				result = size;
			} else {
				int resultArea = result.width * result.height;
				int newArea = size.width * size.height;

				if (newArea < resultArea) {
					result = size;
				}
			}
		}

		return (result);
	}

	private void initPreview(int width, int height) {
		if (camera != null && previewHolder.getSurface() != null) {
			try {
				camera.setPreviewDisplay(previewHolder);
			} catch (Throwable t) {
				Log.e("PreviewDemo-surfaceCallback",
						"Exception in setPreviewDisplay()", t);
				Toast.makeText(HomeActivity.this, t.getMessage(),
						Toast.LENGTH_LONG).show();
			}

			if (!cameraConfigured) {
				Camera.Parameters parameters = camera.getParameters();
				Camera.Size size = getBestPreviewSize(width, height, parameters);
				Camera.Size pictureSize = getSmallestPictureSize(parameters);

				if (size != null && pictureSize != null) {
					parameters.setPreviewSize(size.width, size.height);
					parameters.setPictureSize(pictureSize.width,
							pictureSize.height);
					parameters.setPictureFormat(ImageFormat.JPEG);
					camera.setParameters(parameters);
					cameraConfigured = true;
				}
			}
		}
	}

	private void closeCamera() {
		if (camera != null) {
			camera.stopPreview();
			camera.release();
			camera = null;
		}
	}

	SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
		public void surfaceCreated(SurfaceHolder holder) {
			// no-op -- wait until surfaceChanged()
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			initPreview(width, height);
			try {
				camera.setPreviewDisplay(previewHolder);
				camera.startPreview();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			// no-op
			closeCamera();
		}
	};

	PictureCallback mPicture = new PictureCallback() {
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {

			File pictureFile = getOutputMediaFile();
			if (pictureFile == null) {
				return;
			}
			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				fos.write(data);
				fos.close();
			} catch (FileNotFoundException e) {

			} catch (IOException e) {
			}

			Uri photoUri = Uri.fromFile(pictureFile);

			Intent drawIntent = new Intent(HomeActivity.this,
					DrawingActivity.class);
			drawIntent.setData(photoUri);
			// drawIntent.putExtra("pic", data);
			startActivity(drawIntent);
		}

	};

	private static File getOutputMediaFile() {
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"HelloWorld");
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("HelloWorld", "failed to create directory");
				return null;
			}
		}
		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	class RawCallback implements ShutterCallback, PictureCallback {

		@Override
		public void onShutter() {
			// notify the user, normally with a sound, that the picture has
			// been taken
		}

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			// manipulate uncompressed image data
		}
	}

}