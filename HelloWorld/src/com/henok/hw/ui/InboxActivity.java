package com.henok.hw.ui;

import java.util.ArrayList;
import java.util.List;

import com.henok.hw.R;
import com.henok.hw.R.id;
import com.henok.hw.R.layout;
import com.henok.hw.R.menu;
import com.henok.hw.adapters.MessageAdapter;
import com.henok.hw.utils.ParseConstants;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.os.Build;

public class InboxActivity extends ListActivity {

	protected List<ParseObject> mMessages;
	protected SwipeRefreshLayout mSwipeRefreshLayout;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_inbox);
		
		getActionBar().setIcon(R.drawable.ic_launcher_reverse);

		mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

		mSwipeRefreshLayout
				.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

					@Override
					public void onRefresh() {

						retrieveMessages();

					}
				});

		mSwipeRefreshLayout.setColorScheme(R.color.swipe_refresh_light,
				R.color.white, R.color.swipe_refresh_dark, R.color.white);
		
		
	}

	@Override
	public void onResume() {
		super.onResume();

		setProgressBarIndeterminateVisibility(true);

		retrieveMessages();
	}

	private void retrieveMessages() {
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
				ParseConstants.CLASS_MESSAGES);
		query.whereEqualTo(ParseConstants.KEY_RECIPIENT_IDS, ParseUser
				.getCurrentUser().getObjectId());
		query.addDescendingOrder(ParseConstants.KEY_CREATED_AT);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> messages, ParseException e) {
				setProgressBarIndeterminateVisibility(false);
				
				if(mSwipeRefreshLayout.isRefreshing()){
					mSwipeRefreshLayout.setRefreshing(false);
				}

				if (e == null) {
					// We found messages!
					mMessages = messages;

					String[] usernames = new String[mMessages.size()];
					int i = 0;
					for (ParseObject message : mMessages) {
						usernames[i] = message
								.getString(ParseConstants.KEY_SENDER_NAME);
						i++;
					}

					if (getListView().getAdapter() == null) {
						MessageAdapter adapter = new MessageAdapter(
								getListView().getContext(), mMessages);
						setListAdapter(adapter);
					} else {
						// refill adapter
						((MessageAdapter) getListView().getAdapter())
								.refill(mMessages);
					}
				}
			}
		});
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		ParseObject message = mMessages.get(position);
		String senderUsername = message.getString(ParseConstants.KEY_SENDER_NAME);
		String messageType = message.getString(ParseConstants.KEY_FILE_TYPE);
		ParseFile file = message.getParseFile(ParseConstants.KEY_FILE);
		Uri fileUri = Uri.parse(file.getUrl());

		if (messageType.equals(ParseConstants.TYPE_IMAGE)) {
			// view the image
			Intent intent = new Intent(InboxActivity.this,
					ViewImageActivity.class);
			intent.putExtra("senderUsername", senderUsername);
			intent.setData(fileUri);
			startActivity(intent);
		} else {
			// view the video
			Intent intent = new Intent(Intent.ACTION_VIEW, fileUri);
			intent.setDataAndType(fileUri, "video/*");
			startActivity(intent);
		}

		// Delete it!
		List<String> ids = message.getList(ParseConstants.KEY_RECIPIENT_IDS);

		if (ids.size() == 1) {
			// last recipient - delete the whole thing!
			message.deleteInBackground();
		} else {
			// remove the recipient and save
			ids.remove(ParseUser.getCurrentUser().getObjectId());

			ArrayList<String> idsToRemove = new ArrayList<String>();
			idsToRemove.add(ParseUser.getCurrentUser().getObjectId());

			message.removeAll(ParseConstants.KEY_RECIPIENT_IDS, idsToRemove);
			message.saveInBackground();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();

		switch (itemId) {
		case R.id.menu_setting:
			Intent cameraIntent = new Intent(this, HomeActivity.class);
			startActivity(cameraIntent);
			break;
		case R.id.menu_logout:
			ParseUser.logOut();
			navigateToLogin();
			break;
		case R.id.menu_edit_friends:
			Intent intent = new Intent(this, EditFriendsActivity.class);
			startActivity(intent);
			break;
		case R.id.menu_friends:
			Intent inboxIntent = new Intent(this, FriendsActivity.class);
			startActivity(inboxIntent);
			break;

		}

		return super.onOptionsItemSelected(item);
	}

	private void navigateToLogin() {
		Intent intent = new Intent(this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_inbox,
					container, false);
			return rootView;
		}
	}

}
