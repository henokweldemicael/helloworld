package com.henok.hw.ui;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.os.Build;
import android.provider.MediaStore.Images.Media;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.henok.hw.R;
import com.henok.hw.R.drawable;
import com.henok.hw.R.id;
import com.henok.hw.R.integer;
import com.henok.hw.R.layout;
import com.henok.hw.R.menu;
import com.henok.hw.R.string;
import com.henok.hw.utils.DrawingView;
import com.henok.hw.utils.ParseConstants;
import com.parse.ParseObject;
import com.parse.ParseUser;

import android.provider.MediaStore;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

public class DrawingActivity extends Activity implements OnClickListener {

	private DrawingView drawView;
	private ImageButton currPaint, drawBtn, eraseBtn, newBtn, saveBtn, textBtn;
	private float smallBrush, mediumBrush, largeBrush;
	private String imgSaved;
	protected MenuItem mSendMenuItem;
	private Uri mImageUri;
	private LayoutInflater drawingInflater = null;
	private boolean  isOpen = false;
	private EditText editText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.drawing_view);
		
		
		
		getActionBar().setIcon(R.drawable.ic_launcher_reverse);
		
		drawingInflater = LayoutInflater.from(getBaseContext());
		
		View viewChooseColors = drawingInflater.inflate(
				R.layout.choose_colors, null);
		View viewPhotoEditors = drawingInflater.inflate(R.layout.photo_editors, null);
		LayoutParams layoutParamsControl = new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		
		this.addContentView(viewChooseColors, layoutParamsControl);
		this.addContentView(viewPhotoEditors, layoutParamsControl);
		
		
		// ======================
		drawView = (DrawingView) findViewById(R.id.drawing);

		Matrix matrix = new Matrix();
		matrix.postRotate(ParseConstants.rotation270);

		Matrix matrix2 = new Matrix();
		matrix2.postRotate(ParseConstants.rotation180);

		Uri myUri = getIntent().getData();

		Bitmap original = null;
		try {
			original = Media.getBitmap(this.getContentResolver(), myUri);
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		if ((original.getWidth() > ParseConstants.screenLimit)
				|| (original.getHeight() > ParseConstants.screenLimit)) {
			original = scaleDownBitmap(original,
					ParseConstants.compressionScale, this);
			original = Bitmap.createBitmap(original, 0, 0, original.getWidth(),
					original.getHeight(), matrix2, true);
		}

		original = Bitmap.createBitmap(original, 0, 0, original.getWidth(),
				original.getHeight(), matrix, true);

		@SuppressWarnings("deprecation")
		Drawable background = (Drawable) new BitmapDrawable(original);

		drawView.setBackground(background);
		
		LinearLayout paintLayout = (LinearLayout) findViewById(R.id.paint_colors);
		
		//White paint is the default paint
		currPaint = (ImageButton) paintLayout.getChildAt(0);
		currPaint.setImageDrawable(getResources().getDrawable(
				R.drawable.paint_pressed));

		smallBrush = getResources().getInteger(R.integer.small_size);
		mediumBrush = getResources().getInteger(R.integer.medium_size);
		largeBrush = getResources().getInteger(R.integer.large_size);

		drawBtn = (ImageButton) findViewById(R.id.draw_btn);
		drawBtn.setOnClickListener(this);

		eraseBtn = (ImageButton) findViewById(R.id.erase_btn);
		eraseBtn.setOnClickListener(this);

		newBtn = (ImageButton) findViewById(R.id.new_btn);
		newBtn.setOnClickListener(this);

		saveBtn = (ImageButton) findViewById(R.id.save_btn);
		saveBtn.setOnClickListener(this);
		
		
//		textBtn = (ImageButton) findViewById(R.id.text_btn);
//		textBtn.setOnClickListener(this);

//		
//		editText = (EditText)findViewById(R.id.editScreenText);
//		editText.getText().toString();
//		
//		drawView.addText(editText);

	}

	public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight,
			Context context) {

		final float densityMultiplier = context.getResources()
				.getDisplayMetrics().density;

		int h = (int) (newHeight * densityMultiplier);
		int w = (int) (h * photo.getWidth() / ((double) photo.getHeight()));

		photo = Bitmap.createScaledBitmap(photo, w, h, true);

		return photo;
	}

	public void paintClicked(View view) {
		// use chosen color
		if (view != currPaint) {
			// update color
			ImageButton imgView = (ImageButton) view;
			String color = view.getTag().toString();
			drawView.setColor(color);
			imgView.setImageDrawable(getResources().getDrawable(
					R.drawable.paint_pressed));
			currPaint.setImageDrawable(getResources().getDrawable(
					R.drawable.paint));
			currPaint = (ImageButton) view;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.drawing, menu);

		mSendMenuItem = menu.getItem(0);
		mSendMenuItem.setVisible(true);

		return true;
	}

	@SuppressLint("SimpleDateFormat") @Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.action_send_to_recipients:
			
			if(ParseUser.getCurrentUser() == null){
				AlertDialog.Builder builder = new AlertDialog.Builder(
						DrawingActivity.this);
				builder.setMessage("You need to Login first")
						.setTitle(R.string.signup_error_title)
						.setPositiveButton(
								android.R.string.ok,
								null);
				AlertDialog dialog = builder
						.create();
				dialog.show();
			}else{

			//====
			// save drawing
			drawView.setDrawingCacheEnabled(true);
			// attempt to save
			File mediaStorageDir = new File(
					getExternalFilesDir(Environment.DIRECTORY_PICTURES),
					"HelloWorld");
			if (!mediaStorageDir.exists()) {
				if (!mediaStorageDir.mkdirs()) {
					Log.d("HelloWorld", "failed to create directory");

				}
			}

			String mediaPath = mediaStorageDir.getPath();
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
					.format(new Date());

			imgSaved = MediaStore.Images.Media.insertImage(
					getContentResolver(), drawView.getDrawingCache(),
					mediaPath + File.separator + "IMG_" + timeStamp
							+ ".png", "HelloWorld Drawing");

			mImageUri = Uri.parse(imgSaved);
			// feedback
			if (imgSaved != null) {
				Toast savedToast = Toast.makeText(getApplicationContext(),
						"Drawing saved to Gallery!", Toast.LENGTH_SHORT);
				savedToast.show();
			} else {
				Toast unsavedToast = Toast.makeText(
						getApplicationContext(),
						"Oops! Image could not be saved.",
						Toast.LENGTH_SHORT);
				unsavedToast.show();
			}
			drawView.destroyDrawingCache();

			Intent recipientsIntent = new Intent(DrawingActivity.this,
					RecipientsActivity.class);
			recipientsIntent.setData(mImageUri);
			recipientsIntent.putExtra(ParseConstants.KEY_FILE_TYPE,
					ParseConstants.TYPE_IMAGE);
			startActivity(recipientsIntent);

			//====
			}
			
			return true;
		}

		return super.onOptionsItemSelected(item);

	}

	private void navigateToLogin() {
		Intent intent = new Intent(this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.drawing_view, container,
					false);
			return rootView;
		}
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.draw_btn) {
			// draw button clicked
			final Dialog brushDialog = new Dialog(this);
			brushDialog.setTitle("Brush size:");
			brushDialog.setContentView(R.layout.brush_chooser);
			// listen for clicks on size buttons
			ImageButton smallBtn = (ImageButton) brushDialog
					.findViewById(R.id.small_brush);
			smallBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					drawView.setErase(false);
					drawView.setBrushSize(smallBrush);
					drawView.setLastBrushSize(smallBrush);
					brushDialog.dismiss();
				}
			});
			ImageButton mediumBtn = (ImageButton) brushDialog
					.findViewById(R.id.medium_brush);
			mediumBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					drawView.setErase(false);
					drawView.setBrushSize(mediumBrush);
					drawView.setLastBrushSize(mediumBrush);
					brushDialog.dismiss();
				}
			});
			ImageButton largeBtn = (ImageButton) brushDialog
					.findViewById(R.id.large_brush);
			largeBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					drawView.setErase(false);
					drawView.setBrushSize(largeBrush);
					drawView.setLastBrushSize(largeBrush);
					brushDialog.dismiss();
				}
			});
			// show and wait for user interaction
			brushDialog.show();
		} else if (v.getId() == R.id.erase_btn) {
			// switch to erase - choose size

			final Dialog brushDialog = new Dialog(this);
			brushDialog.setTitle("Eraser size:");
			brushDialog.setContentView(R.layout.brush_chooser);
			// size buttons
			ImageButton smallBtn = (ImageButton) brushDialog
					.findViewById(R.id.small_brush);
			smallBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					drawView.setErase(true);
					drawView.setBrushSize(smallBrush);
					brushDialog.dismiss();
				}
			});
			ImageButton mediumBtn = (ImageButton) brushDialog
					.findViewById(R.id.medium_brush);
			mediumBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					drawView.setErase(true);
					drawView.setBrushSize(mediumBrush);
					brushDialog.dismiss();
				}
			});
			ImageButton largeBtn = (ImageButton) brushDialog
					.findViewById(R.id.large_brush);
			largeBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					drawView.setErase(true);
					drawView.setBrushSize(largeBrush);
					brushDialog.dismiss();
				}
			});
			brushDialog.show();

		} else if (v.getId() == R.id.new_btn) {
			// new button
			AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
			newDialog.setTitle(R.string.new_drawing_title);
			newDialog.setMessage(R.string.new_drawing_message);
			newDialog.setPositiveButton(android.R.string.yes,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							drawView.startNew();
							dialog.dismiss();
						}
					});
			newDialog.setNegativeButton(android.R.string.cancel,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			newDialog.show();
		} else if (v.getId() == R.id.save_btn) {
			// save drawing
			AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);
			saveDialog.setTitle("Save drawing");
			saveDialog.setMessage("Save drawing to device Gallery?");
			saveDialog.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// save drawing
							drawView.setDrawingCacheEnabled(true);
							// attempt to save
							File mediaStorageDir = new File(
									Environment
											.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
									"HelloWorld");
							if (!mediaStorageDir.exists()) {
								if (!mediaStorageDir.mkdirs()) {
									Log.d("HelloWorld",
											"failed to create directory");

								}
							}

							String mediaPath = mediaStorageDir.getPath();
							String timeStamp = new SimpleDateFormat(
									"yyyyMMdd_HHmmss").format(new Date());

							imgSaved = MediaStore.Images.Media.insertImage(
									getContentResolver(),
									drawView.getDrawingCache(), mediaPath
											+ File.separator + "IMG_"
											+ timeStamp + ".png",
									"HelloWorld Drawing");

							mImageUri = Uri.parse(imgSaved);
							// feedback
							if (imgSaved != null) {
								Toast savedToast = Toast.makeText(
										getApplicationContext(),
										"Drawing saved to Gallery!",
										Toast.LENGTH_SHORT);
								savedToast.show();
							} else {
								Toast unsavedToast = Toast.makeText(
										getApplicationContext(),
										"Oops! Image could not be saved.",
										Toast.LENGTH_SHORT);
								unsavedToast.show();
							}
							drawView.destroyDrawingCache();
						}
					});
			saveDialog.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			saveDialog.show();
			
		}
//		else if(v.getId() == R.id.text_btn){
//			//TODO
//			
//			if(isOpen == false){
//				editText.setVisibility(View.VISIBLE);
//				isOpen = true;
//			}else{
//				editText.setVisibility(View.INVISIBLE);
//				isOpen = false;
//			}
//			
//		}

	}

}
