package com.henok.hw.ui;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.henok.hw.R;
import com.henok.hw.R.id;
import com.henok.hw.R.layout;
import com.henok.hw.R.menu;
import com.henok.hw.R.string;
import com.henok.hw.adapters.UserAdapter;
import com.henok.hw.utils.ParseConstants;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class EditFriendsActivity extends Activity {

	protected ParseRelation<ParseUser> mFriendsRelation;
	protected ParseUser mCurrentUser;
	protected EditText mSearch;
	protected Button mGo;
	protected TextView mNoResult;
	String searchThisUser;

	public static final String TAG = EditFriendsActivity.class.getSimpleName();

	protected List<ParseUser> mUsers;
	protected GridView mGridView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_edit_friends);
		// Show the Up button in the action bar.
		setupActionBar();
		
		getActionBar().setIcon(R.drawable.ic_launcher_reverse);

		mGridView = (GridView) findViewById(R.id.friendsGrid);

		mGridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
		mGridView.setOnItemClickListener(mOnItemClickListener);
	}

	@Override
	protected void onResume() {
		super.onResume();

		mCurrentUser = ParseUser.getCurrentUser();
		mFriendsRelation = mCurrentUser
				.getRelation(ParseConstants.KEY_FRIENDS_RELATION);

		mSearch = (EditText) findViewById(R.id.search);

		mNoResult = (TextView) findViewById(R.id.noResults);

		mGo = (Button) findViewById(R.id.goButton);
		mGo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				searchThisUser = mSearch.getText().toString();
				searchThisUser = searchThisUser.trim();

				// Search for the typed username

				ParseQuery<ParseUser> query = ParseUser.getQuery();
				// query.orderByAscending(ParseConstants.KEY_USERNAME);

				query.whereEqualTo(ParseConstants.KEY_USERNAME, ""
						+ searchThisUser);
				// query.setLimit(100);
				query.findInBackground(new FindCallback<ParseUser>() {
					@Override
					public void done(List<ParseUser> users, ParseException e) {
						setProgressBarIndeterminateVisibility(false);

						// No results returned
						if (users.size() == 0) {
							mNoResult.setVisibility(View.VISIBLE);
						}else{
							mNoResult.setVisibility(View.INVISIBLE);
						}

						if (e == null) {
							// Success
							
							mUsers = users;
							String[] usernames = new String[mUsers.size()];
							int i = 0;
							for (ParseUser user : mUsers) {
								usernames[i] = user.getUsername();
								i++;
							}
							if (mGridView.getAdapter() == null) {
								UserAdapter adapter = new UserAdapter(
										EditFriendsActivity.this, mUsers);
								// setListAdapter(adapter);
								mGridView.setAdapter(adapter);
							} else {
								((UserAdapter) mGridView.getAdapter())
										.refill(mUsers);
							}

							addFriendCheckmarks();
						} else {
							
							Log.e(TAG, e.getMessage());
							AlertDialog.Builder builder = new AlertDialog.Builder(
									EditFriendsActivity.this);
							builder.setMessage(e.getMessage())
									.setTitle(R.string.error_title)
									.setPositiveButton(android.R.string.ok,
											null);
							AlertDialog dialog = builder.create();
							dialog.show();
						}
					}
				});

			}
		});
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	public void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.main, menu);

		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// if (item.getItemId() == R.id.camera) {
		// if (inPreview) {
		// camera.takePicture(null, null, photoCallback);
		// inPreview = false;
		// }
		// }
		//
		// return (super.onOptionsItemSelected(item));
		int itemId = item.getItemId();

		switch (itemId) {
		case R.id.menu_setting:
			Intent cameraIntent = new Intent(this, SettingActivity.class);
			startActivity(cameraIntent);
			break;
		case R.id.menu_logout:
			ParseUser.logOut();
			navigateToLogin();
			break;
		case R.id.menu_edit_friends:
			Intent intent = new Intent(this, EditFriendsActivity.class);
			startActivity(intent);
			break;
		case R.id.menu_friends:
			Intent friendsIntent = new Intent(this, FriendsActivity.class);
			startActivity(friendsIntent);
			break;

		case R.id.menu_inbox:
			Intent inboxIntent = new Intent(this, InboxActivity.class);
			startActivity(inboxIntent);
			break;

		}

		return super.onOptionsItemSelected(item);
	}

	private void navigateToLogin() {
		Intent intent = new Intent(this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}

	private void addFriendCheckmarks() {
		mFriendsRelation.getQuery().findInBackground(
				new FindCallback<ParseUser>() {
					@Override
					public void done(List<ParseUser> friends, ParseException e) {
						if (e == null) {
							// list returned - look for a match
							for (int i = 0; i < mUsers.size(); i++) {
								ParseUser user = mUsers.get(i);

								for (ParseUser friend : friends) {
									if (friend.getObjectId().equals(
											user.getObjectId())) {
										mGridView.setItemChecked(i, true);
									}
								}
							}
						} else {
							Log.e(TAG, e.getMessage());
						}
					}
				});
	}

	protected OnItemClickListener mOnItemClickListener = new GridView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long arg3) {
			ImageView checkImageView = (ImageView) view
					.findViewById(R.id.checkedImageView);
			

			if (mGridView.isItemChecked(position)) {
				// add the friend
				mFriendsRelation.add(mUsers.get(position));
				checkImageView.setVisibility(View.VISIBLE);
			} else {
				// remove the friend
				mFriendsRelation.remove(mUsers.get(position));
				checkImageView.setVisibility(View.INVISIBLE);
			}

			mCurrentUser.saveInBackground(new SaveCallback() {
				@Override
				public void done(ParseException e) {
					if (e != null) {
						Log.e(TAG, e.getMessage());
					}
				}
			});

		}
	};
}
