package com.henok.hw.ui;

import com.henok.hw.R;
import com.henok.hw.R.id;
import com.henok.hw.R.layout;
import com.henok.hw.R.menu;
import com.henok.hw.utils.ParseConstants;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.os.Build;

public class RequestEmailActivity extends Activity {

	protected EditText mEmail;
	protected EditText mCityAndCountry;
	protected Button mStart;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_request_email);

		mEmail = (EditText) findViewById(R.id.emailField);
		mCityAndCountry = (EditText) findViewById(R.id.cityAndCountry);
		mStart = (Button) findViewById(R.id.startButton);

		mStart.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				final String email = mEmail.getText().toString().toLowerCase()
						.trim();
				final String cityAndCountry = mCityAndCountry.getText()
						.toString();

				if (email.isEmpty() || cityAndCountry.isEmpty()) {
					// ERROR
					AlertDialog.Builder builder = new AlertDialog.Builder(
							RequestEmailActivity.this);
					builder.setMessage(R.string.request_email_error_message)
							.setTitle(R.string.login_error_title)
							.setPositiveButton(android.R.string.ok, null);
					AlertDialog dialog = builder.create();
					dialog.show();
				} else {
				
					
					// Update information
					ParseUser currentUser = ParseUser.getCurrentUser();
					currentUser.setEmail(email);
					currentUser.put(ParseConstants.USER_LOCATION,
							cityAndCountry);
					currentUser.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {
							Intent intent = new Intent(RequestEmailActivity.this,
									HomeActivity.class);
							
							intent.putExtra("email", email);
							intent.putExtra("location", cityAndCountry);

							startActivity(intent);

						}
					});

				

				}

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.request_email, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.activity_request_email,
					container, false);
			return rootView;
		}
	}

}
