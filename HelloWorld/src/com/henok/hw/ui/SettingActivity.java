package com.henok.hw.ui;

import com.henok.hw.R;
import com.henok.hw.R.id;
import com.henok.hw.R.layout;
import com.henok.hw.R.menu;
import com.henok.hw.utils.ParseConstants;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.os.Build;

public class SettingActivity extends Activity {

	private int selectedLimit;
	private ParseUser currentUser;
	private NumberPicker limitPicker;
	private EditText changeLocation;
	private Button saveLocationBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);

		getActionBar().setIcon(R.drawable.ic_launcher_reverse);

		currentUser = ParseUser.getCurrentUser();

		limitPicker = (NumberPicker) findViewById(R.id.limitPicker);
		limitPicker.setMinValue(1);
		limitPicker.setMaxValue(ParseConstants.strangersLimit);
		limitPicker.setValue(ParseConstants.strangersLimit);
		limitPicker.setWrapSelectorWheel(true);
		limitPicker
				.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

					@Override
					public void onValueChange(NumberPicker picker, int oldVal,
							int newVal) {
						selectedLimit = newVal;

						currentUser.remove(ParseConstants.HELLO_LIMIT);

						currentUser.add(ParseConstants.HELLO_LIMIT,
								selectedLimit);

						limitPicker.setValue(selectedLimit);

						currentUser.saveInBackground(new SaveCallback() {

							@Override
							public void done(ParseException arg0) {
								Toast.makeText(SettingActivity.this,
										("Limit was set to " + selectedLimit),
										Toast.LENGTH_LONG).show();

							}
						});
					}
				});

		changeLocation = (EditText) findViewById(R.id.updateLocationField);
		saveLocationBtn = (Button) findViewById(R.id.saveLocationButton);
		saveLocationBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				final String newLocation = changeLocation.getText().toString();
				System.out.println(newLocation);
				currentUser.remove(ParseConstants.USER_LOCATION);

				currentUser
						.add(ParseConstants.USER_LOCATION, newLocation);
				currentUser.saveInBackground(new SaveCallback() {

					@Override
					public void done(ParseException e) {
						if (e == null) {
							Toast.makeText(SettingActivity.this,
									"New Location: " + newLocation,
									Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(SettingActivity.this,
									R.string.error_location_was_not_updated,
									Toast.LENGTH_LONG).show();
							Log.e("My App", e.toString());
						}

					}
				});

			}
		});

	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.main, menu);

		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// if (item.getItemId() == R.id.camera) {
		// if (inPreview) {
		// camera.takePicture(null, null, photoCallback);
		// inPreview = false;
		// }
		// }
		//
		// return (super.onOptionsItemSelected(item));
		int itemId = item.getItemId();

		switch (itemId) {
		case R.id.menu_setting:
			Intent cameraIntent = new Intent(this, SettingActivity.class);
			startActivity(cameraIntent);
			break;
		case R.id.menu_logout:
			ParseUser.logOut();
			navigateToLogin();
			break;
		case R.id.menu_edit_friends:
			Intent intent = new Intent(this, EditFriendsActivity.class);
			startActivity(intent);
			break;
		case R.id.menu_friends:
			Intent inboxIntent = new Intent(this, FriendsActivity.class);
			startActivity(inboxIntent);
			break;

		}

		return super.onOptionsItemSelected(item);
	}

	private void navigateToLogin() {
		Intent intent = new Intent(this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.activity_setting,
					container, false);
			return rootView;
		}
	}

}
