package com.henok.hw.ui;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.henok.hw.HWApplication;
import com.henok.hw.R;
import com.henok.hw.R.id;
import com.henok.hw.R.layout;
import com.henok.hw.R.string;
import com.henok.hw.utils.ParseConstants;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.parse.SaveCallback;
import com.parse.twitter.Twitter;

public class LoginActivity extends Activity {

	protected EditText mUsername;
	protected EditText mPassword;
	protected Button mLoginButton;
	//protected Button mFacebookLogin;
	//protected Button mTwitterLogin;

	protected String email;
	protected EditText input;

	protected TextView mForgotPasswordTextView;
	protected TextView mSignUpTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_login);
		
		if(ParseUser.getCurrentUser() != null){
			Intent in = new Intent(LoginActivity.this, HomeActivity.class);
			startActivity(in);
		}

		ActionBar actionBar = getActionBar();
		actionBar.hide();

		mSignUpTextView = (TextView) findViewById(R.id.signUpText);
		mSignUpTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this,
						SignUpActivity.class);
				startActivity(intent);
			}
		});

		mForgotPasswordTextView = (TextView) findViewById(R.id.forgotPasswordText);
		mForgotPasswordTextView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				// ask for email address
				AlertDialog.Builder builder = new AlertDialog.Builder(
						LoginActivity.this);
				builder.setTitle(R.string.input_email_title);

				// Set up the input
				input = new EditText(LoginActivity.this);
				// Specify the type of input expected; this, for example, sets
				// the input as a password, and will mask the text
				input.setInputType(InputType.TYPE_CLASS_TEXT);
				builder.setView(input);

				// Set up the buttons
				builder.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								email = input.getText().toString();

								// reset the password
								ParseUser.requestPasswordResetInBackground(
										email,
										new RequestPasswordResetCallback() {
											public void done(ParseException e) {
												if (e == null) {
													// An email was successfully
													// sent with reset
													// instructions.
													AlertDialog.Builder builder = new AlertDialog.Builder(
															LoginActivity.this);
													builder.setMessage(
															e.getMessage())
															.setMessage(
																	R.string.password_reset_email_sent)
															.setTitle(
																	R.string.password_reset_email_sent_label)
															.setPositiveButton(
																	android.R.string.ok,
																	null);
													AlertDialog dialog = builder
															.create();
													dialog.show();
												} else {
													// Something went wrong.
													// Look at the
													// ParseException to see
													// what's up.
													AlertDialog.Builder builder = new AlertDialog.Builder(
															LoginActivity.this);
													builder.setMessage(
															e.getMessage())
															.setTitle(
																	R.string.login_error_title)
															.setPositiveButton(
																	android.R.string.ok,
																	null);
													AlertDialog dialog = builder
															.create();
													dialog.show();
												}
											}
										});
							}
						});
				builder.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();
							}
						});

				builder.show();

			}
		});

		mUsername = (EditText) findViewById(R.id.usernameField);
		mPassword = (EditText) findViewById(R.id.passwordField);
		mLoginButton = (Button) findViewById(R.id.loginButton);
		mLoginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String username = mUsername.getText().toString();
				String password = mPassword.getText().toString();

				username = username.trim();
				password = password.trim();

				if (username.isEmpty() || password.isEmpty()) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							LoginActivity.this);
					builder.setMessage(R.string.login_error_message)
							.setTitle(R.string.login_error_title)
							.setPositiveButton(android.R.string.ok, null);
					AlertDialog dialog = builder.create();
					dialog.show();
				} else {
					// Login
					setProgressBarIndeterminateVisibility(true);

					ParseUser.logInInBackground(username, password,
							new LogInCallback() {
								@Override
								public void done(ParseUser user,
										ParseException e) {
									setProgressBarIndeterminateVisibility(false);

									if (e == null) {
										// Success!
										HWApplication.updateParseInstallation(user);
										Intent intent = new Intent(
												LoginActivity.this,
												HomeActivity.class);
										intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
										startActivity(intent);
									} else {
										AlertDialog.Builder builder = new AlertDialog.Builder(
												LoginActivity.this);
										builder.setMessage(e.getMessage())
												.setTitle(
														R.string.login_error_title)
												.setPositiveButton(
														android.R.string.ok,
														null);
										AlertDialog dialog = builder.create();
										dialog.show();
									}
								}
							});
				}
			}
		});
//
//		mFacebookLogin = (Button) findViewById(R.id.facebookLoginBtn);
//		mFacebookLogin.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				// FACEBOOK
//				List<String> permissions = Arrays.asList("public_profile",
//						"user_friends", "email", "user_location");
//				ParseFacebookUtils.logIn(permissions, LoginActivity.this,
//						new LogInCallback() {
//							@Override
//							public void done(final ParseUser user,
//									ParseException err) {
//								if (user == null) {
//									Log.d("MyApp",
//											"Uh oh. The user cancelled the Facebook login.");
//								} else {
//									Log.d("MyApp",
//											"User signed up and logged in through Facebook!");
//
//									Session session = ParseFacebookUtils
//											.getSession();
//									if (session != null && session.isOpened()) {
//										Request request = Request.newMeRequest(
//												ParseFacebookUtils.getSession(),
//												new Request.GraphUserCallback() {
//													@Override
//													public void onCompleted(
//															GraphUser facebookUser,
//															Response response) {
//														String email = facebookUser
//																.getId();
//														String username = facebookUser
//																.getUsername();
//														String location = "unknown";
//														if (facebookUser
//																.getLocation()
//																.getProperty(
//																		"name") != null) {
//
//															location = (String) facebookUser
//																	.getLocation()
//																	.getProperty(
//																			"name");
//														}
//
//														user.setUsername(username);
//														user.setEmail(email);
//														user.put(
//																ParseConstants.USER_LOCATION,
//																location);
//														user.saveInBackground(new SaveCallback() {
//
//															@Override
//															public void done(
//																	ParseException e) {
//
//																Log.d("MyApp",
//																		"Parse Facebook user created!");
//															}
//														});
//
//														Intent intent = new Intent(
//																LoginActivity.this,
//																HomeActivity.class);
//														startActivity(intent);
//
//													}
//												});
//										request.executeAsync();
//									}
//
//									// } else {
//									// Log.d("MyApp",
//									// "User logged in through Facebook!");
//									//
//								}
//							}
//
//						});
//				// FACEBOOK
//
//			}
//		});

		// mTwitterLogin = (Button) findViewById(R.id.twitterLoginBtn);
		// mTwitterLogin.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		//
		// // Twitter
		// // ParseTwitterUtils.logIn(LoginActivity.this,
		// // new LogInCallback() {
		// // @Override
		// // public void done(ParseUser user, ParseException err) {
		// // if (user == null) {
		// // Log.d("MyApp",
		// // "Uh oh. The user cancelled the Twitter login.");
		// // } else if (user.isNew()) {
		// // Log.d("MyApp",
		// // "User signed up and logged in through Twitter!");
		// // Twitter twitterUser = ParseTwitterUtils
		// // .getTwitter();
		// // String username = twitterUser
		// // .getScreenName();
		// //
		// //
		// // user.setUsername(username);
		// // user.saveInBackground(new SaveCallback() {
		// //
		// // @Override
		// // public void done(ParseException e) {
		// //
		// // Log.d("MyApp",
		// // "Parse username has been set to Twitter handle");
		// // }
		// // });
		// //
		// // Intent intent = new Intent(
		// // LoginActivity.this,
		// // RequestEmailActivity.class);
		// // startActivity(intent);
		// // } else {
		// // Log.d("MyApp",
		// // "User logged in through Twitter!");
		// //
		// // Twitter twitterUser = ParseTwitterUtils
		// // .getTwitter();
		// // String username = twitterUser
		// // .getScreenName();
		// //
		// // user.setUsername(username);
		// // user.saveInBackground(new SaveCallback() {
		// //
		// // @Override
		// // public void done(ParseException e) {
		// //
		// // Log.d("MyApp",
		// // "Parse username has been set to Twitter handle");
		// // }
		// // });
		// //
		// // Intent intent = new Intent(
		// // LoginActivity.this,
		// // RequestEmailActivity.class);
		// // startActivity(intent);
		// // }
		// // }
		// //
		// // });
		// // TWITTER
		// }
		// });

	}
}
