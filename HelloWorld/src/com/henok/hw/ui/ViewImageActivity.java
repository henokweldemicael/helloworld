package com.henok.hw.ui;

import java.util.Timer;
import java.util.TimerTask;

import com.henok.hw.R;
import com.henok.hw.R.id;
import com.henok.hw.R.layout;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class ViewImageActivity extends Activity {
	ProgressBar progress;
	CountDownTimer mCountDownTimer;
	int i = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_image);
		// Show the Up button in the action bar.
		setupActionBar();

		getActionBar().setIcon(R.drawable.ic_launcher_reverse);

		String senderUsername = getIntent().getExtras().getString(
				"senderUsername");
		setTitle(senderUsername);

		progress = (ProgressBar) findViewById(R.id.progressBar);
		ImageView imageView = (ImageView) findViewById(R.id.imageView);

		progress.setProgress(i);
		mCountDownTimer = new CountDownTimer(30 * 1000, 1 * 300) {

			@Override
			public void onTick(long millisUntilFinished) {
				i++;
				progress.setProgress(i);

			}

			@Override
			public void onFinish() {
				// Do what you want
				i++;
				progress.setProgress(i);
				finish();
			}
		};
		mCountDownTimer.start();

		Uri imageUri = getIntent().getData();

		Picasso.with(this).load(imageUri.toString()).into(imageView);

		// Timer timer = new Timer();
		//
		// timer.schedule(new TimerTask() {
		//
		// @Override
		// public void run(){
		//
		// finish();
		// }
		// }, 10*1000);

	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
