package com.henok.hw.utils;

public final class ParseConstants {
	// Class name
	public static final String CLASS_MESSAGES = "Messages";
	
	// Field names
	public static final String KEY_USER_ID = "userId";
	public static final String KEY_USERNAME = "username";
	public static final String KEY_FRIENDS_RELATION = "friendsRelation";
	public static final String KEY_RECIPIENT_IDS = "recipientIds";
	public static final String KEY_SENDER_ID = "senderId";
	public static final String KEY_SENDER_NAME = "senderName";
	public static final String KEY_FILE = "file";
	public static final String KEY_FILE_TYPE = "fileType";
	public static final String KEY_CREATED_AT = "createdAt";
	public static final String TYPE_IMAGE = "image";
	public static final String TYPE_VIDEO = "video";
	public static final int rotation180 = 180;
	public static final int rotation270 = 270;
	public static final int compressionScale = 200;
	public static final int screenLimit = 2000;
	public static final int senderLimit = 2;
	public static final int receiveLimit = 2;
	public static final int strangersDefault = 10;
	public static final int strangersLimit = 30;
	public static final String HELLO_LIMIT = "helloLimit";
	public static final String HELLOS_RECEIVED = "hellosReceived";
	public static final String USER_LOCATION = "userLocation";
	public static final String SENDER_LOCATION = "senderLocation";
	public static final String MY_LOCATION = "myLocation";
	
	
}
